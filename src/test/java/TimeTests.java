import static org.junit.Assert.assertEquals;

import org.junit.Test;

import environment.time.Day;
import environment.time.Time;

public class TimeTests {
    @Test
    public void toStringRepresentation(){
        Time t1 = new Time(Day.MONDAY, 10, 5);
        assertEquals("Monday 10:05", t1.toString());
        Time t2 = new Time(Day.FRIDAY, 8, 55);
        assertEquals("Friday 08:55", t2.toString());
    }
    @Test
    public void addTimeTillNewDay(){
        Time t1 = new Time(Day.FRIDAY, 23, 5);
        Time t2 = t1.addTime(0, 55);
        assertEquals("Saturday 00:00", t2.toString());
    }
    @Test
    public void subtractTimeToPreviousDay(){
        Time t1 = new Time(Day.FRIDAY, 1, 5);
        Time t2 = t1.subtractTime(1, 6);
        assertEquals("Thursday 23:59", t2.toString());
    }
    @Test
    public void addTimeTillAfterTomorrow(){
        Time t1 = new Time(Day.FRIDAY, 0, 0);
        Time t2 = t1.addTime(48, 5);
        assertEquals("Monday 00:05", t2.toString());
        Time t3 = new Time(Day.FRIDAY, 0, 0);
        Time t4 = t3.addTime(47, 59);
        assertEquals("Sunday 23:59", t4.toString());
    }
}
