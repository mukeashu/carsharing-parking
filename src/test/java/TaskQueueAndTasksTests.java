import static org.junit.Assert.assertEquals;
import org.junit.Test;
import cars.tasks.*;

public class TaskQueueAndTasksTests {
    @Test
    public void creatingTaskQueueWithTask(){
        TaskQueue queue = new TaskQueue(16);
        Task task = new WaitTask(10);
        queue.offer(task);

        assertEquals(task, queue.peek());
    }
    @Test
    public void emptyingTaskQueueWtihClearTasks(){
        TaskQueue queue = new TaskQueue(16);
        Task task = new WaitTask(10);
        queue.offer(task);
        assertEquals(task, queue.peek());
        queue.clearTasks();
        assertEquals(null, queue.peek());
    }
    @Test
    public void emptyingTaskQueueWtihClear(){
        TaskQueue queue = new TaskQueue(16);
        Task task = new WaitTask(10);
        queue.offer(task);
        assertEquals(task, queue.peek());
        queue.clear();
        assertEquals(null, queue.peek());
    }
    @Test
    public void wrappersGetTaskAndAddWaitTask(){
        TaskQueue queue = new TaskQueue(16);
        queue.addWaitTask(10);
        assertEquals(10, queue.getTask().getDuration());
    }
    @Test 
    public void checkingTaskQueueSize(){
        TaskQueue queue = new TaskQueue(16);
        Task task = new WaitTask(10);
        queue.offer(task);

        assertEquals(1, queue.size());
    }
    @Test
    public void stabilityAtMultipleOffersAndPolls1(){
        TaskQueue queue = new TaskQueue(4);
        queue.offer(new PaidParkingTask(10));
        queue.offer(new PaidParkingTask(20));
        queue.offer(new WaitTask(30));
        queue.offer(new WaitTask(40));

        assertEquals(4, queue.size());

        assertEquals(10, queue.peek().getDuration());

        assertEquals(false, queue.offer(new PaidParkingTask(60)));
    }
    @Test
    public void stabilityAtMultipleOffersAndPolls2(){
        TaskQueue queue = new TaskQueue(4);
        queue.offer(new PaidParkingTask(10));
        queue.offer(new PaidParkingTask(20));
        queue.offer(new WaitTask(30));
        queue.offer(new WaitTask(40));

        assertEquals(4, queue.size());

        assertEquals(10, queue.poll().getDuration());

        assertEquals(3, queue.size());

        queue.offer(new PaidParkingTask(50));

        assertEquals(4, queue.size());

        assertEquals(false, queue.offer(new PaidParkingTask(60)));
    }
    @Test
    public void tasksTimeDecrementationOnIsFinished(){
        Task task = new WaitTask(5);
        assertEquals(false, task.isFinished());
        assertEquals(1, task.getTimeSpent());
    }
}
