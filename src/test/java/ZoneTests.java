import static org.junit.Assert.assertEquals;
import org.junit.Test;
import environment.spatial.Zone;

public class ZoneTests {

    @Test
    public void nameTest(){
        assertEquals("'KrakowA'", Zone.A.toString());
        assertEquals("'KrakowB'", Zone.B.toString());
        assertEquals("'KrakowC'", Zone.C.toString());
    }
    @Test
    public void priceTest(){
        Zone zone1 = Zone.A;
        assertEquals(zone1.getPrice(), 6.0, 0.005);
        Zone zone2 = Zone.B;
        assertEquals(zone2.getPrice(), 5.0, 0.005);
        Zone zone3 = Zone.C;
        assertEquals(zone3.getPrice(), 4.0, 0.005);
    }
}
