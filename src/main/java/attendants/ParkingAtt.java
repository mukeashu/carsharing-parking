package attendants;

import cars.Car;

import java.util.Random;

public class ParkingAtt{

    private ParkingAttController controller;
    private Car currentCar = null;
    private int timeToWait = 0;
    private Random rand = new Random(System.currentTimeMillis());

    private void giveTicket(Car car) {
        car.fineForParking();
        System.out.println("Ticket have been given to car" + car.getId());
    }


    public void processCar(){

        if (currentCar.isDriving()) {
            timeToWait = 0;
            return;
        }
        if (currentCar.getParkingStatus()) {
            timeToWait = 0;
            return;
        } else if (timeToWait > 0) {
            if (timeToWait == 1) {
                System.out.println("Car is given a ticket");
                giveTicket(currentCar);
                this.currentCar = null;
            }
            timeToWait--;
        } else {
            timeToWait = 5;
        }
    }

    public void proceed(Car car){
        int random = rand.nextInt(100);

        if (currentCar==null) {
            this.currentCar = car;
        }

        if (random == 0){
            //roll a 1% to get checked
            if(car.getParkingStatus()) {  //TODO 'random == 0' in order to have 1% chance
                currentCar = null;
            }
        }
    }


    public Car getCurrentCar() {
        return currentCar;
    }

}
