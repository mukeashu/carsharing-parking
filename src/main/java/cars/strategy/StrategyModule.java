package cars.strategy;

import cars.tasks.PaidParkingTask;
import cars.tasks.Task;
import environment.ParkingMeter;
import environment.spatial.Point;
import environment.spatial.Zone;
import environment.time.Time;


public abstract class StrategyModule{
    /**
     * A class used by the strategy modules to add an extra information about a max
     * payment expected for a given task. That information is used later by a call
     * to ParkingMeter to make sure that the time provided in the task makes sense
     * with the given payment.
     * @see ParkingMeter 
     * @see ParkingMeter#validate(Zone, Time, Task, double)
     */
    class TaskAndPayment{
        public Task task;
        public double maxPayment; 
        public TaskAndPayment(Task task, double maxPayment){
            this.task = task; this.maxPayment = maxPayment;
        }
    }

    /**
     * The inner workings of a chosen strategy modules. 
     * @return It must return an array that joins each task with an expected maximal payment.
     */
    protected abstract TaskAndPayment[] createTasks(Point carLocation, boolean fineState, Time time);
    
    public abstract void informStrategyOfPayment(Point carLocation, Time time, double payment);

    /**
     * A protection layer for each strategy module that will check that the rules of simulation
     * are not violated by the strategy. Cars asking for new tasks communicate through this method.
     */
    public final Task[] giveTasks(Point carLocation, boolean fineState, Time time){
        TaskAndPayment[] generatedTasks = createTasks(carLocation, fineState, time);
        for(int i = 0; i < generatedTasks.length; i++){
            if(generatedTasks[i].task instanceof PaidParkingTask){ //WaitTasks don't set parkingPaid flag in Car so we don't care
                boolean isValid = ParkingMeter.validate(
                    carLocation.getZone(),
                    time,
                    generatedTasks[i].task, 
                    generatedTasks[i].maxPayment
                );
                if(!isValid){ //With invalid strategy the simulation does not make sense
                    System.out.println("The chosen strategy module broke rules of simulation!");
                    System.exit(1);
                }
            }
        }
        Task[] tasks = new Task[generatedTasks.length];
        for(int i = 0; i < tasks.length; i++){
            tasks[i] = generatedTasks[i].task;
        }
        return tasks;
    }
}
