package cars.tasks;

public class PaidParkingTask implements Task{
    private int timeLeft;
    private int duration;
    public PaidParkingTask(int duration){ 
        this.timeLeft = duration;
        this.duration = duration;
    }
    public boolean isFinished(){
        return (this.timeLeft-- == 0);
    }
    /**
     * The information about the time spent on
     * PaidParkingTask can be used to control 
     * start-stop feature of "SkyCash"
     */
    public int getTimeSpent(){
        return duration - timeLeft;
    }
    public int getDuration(){
        return duration;
    }
}
