package cars.tasks;

public class WaitTask implements Task{
    private int timeLeft;
    private int duration;
    public WaitTask(int duration){
        this.timeLeft = duration;
        this.duration = duration;
    }
    public boolean isFinished(){
        return (this.timeLeft-- == 0);
    }
    /**
     * In case of WaitTask the information about
     * the time spent waiting might be used to check if
     * the car has been parked without payment for more
     * than five minutes.
     */
    public int getTimeSpent(){
        return duration - timeLeft;
    }
    public int getDuration(){
        return duration;
    }
}

