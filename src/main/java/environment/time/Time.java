package environment.time;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.security.InvalidParameterException;

/**
 * A wrapper class that makes passing current time between objects easier.
 */
@Embeddable
public class Time{
    private String dayInString;
    private int hour;
    private int minutes;
    @Transient
    private Day day;

    public Time(Day day, int hours, int minutes){
        this.day = day; this.hour = hours; this.minutes = minutes; this.dayInString = this.day.toString();
    }

    public Time() {

    }

    public Day getDay(){
        return day;
    }
    public int getHour(){
        return hour;
    }
    public int getMinutes(){
        return minutes;
    }
    public String getDayInString() {
        return dayInString;
    }

    public void setDayInString(String dayInString) {
        this.dayInString = dayInString;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setDay(Day day) {
        this.day = day;
    }


    public String toString(){
        String minutesString;
        String hoursString;
        if(this.minutes < 10) minutesString = "0" + this.minutes;
        else minutesString = "" + this.minutes;
        if(this.hour < 10) hoursString = "0" + this.hour;
        else hoursString = "" + this.hour;

        return this.day.toString() + " " + hoursString + ":" + minutesString;
    }

    public Time addTime(int hours, int minutes){
        if(minutes > 60) throw new InvalidParameterException("Cannot specify more minutes than 60.");
        Day newDay = this.getDay();
        int newHour = this.getHour();
        int newMinute = this.getMinutes();

        if(newMinute + minutes >= 60){
            newHour++;
            newMinute = (newMinute + minutes) % 60;
        }
        else
            newMinute += minutes;

        if(newHour + hours >= 24){
            newDay = newDay.next();
            for(int i = hours / 24; i > 0; i--)
                newDay = newDay.next();
            newHour = (newHour + hours) % 24;
        }
        else
            newHour += hours;

        return new Time(newDay, newHour, newMinute);
    }
    public Time addTime(int minutes){
        int hours = minutes / 60;
        return addTime(hours, minutes % 60);
    }

    public Time subtractTime(int hours, int minutes){
        if(minutes > 60) throw new InvalidParameterException("Cannot specify more minutes than 60.");
        Day newDay = this.getDay();
        int newHour = this.getHour();
        int newMinute = this.getMinutes();

        if(newMinute - minutes < 0){
            newHour--;
            newMinute = 60 - (minutes - newMinute);
        }
        else
            newMinute -= minutes;

        if(newHour - hours < 0){
            newDay = newDay.previous();
            for(int i = hours / 24; i > 0; i--)
                newDay = newDay.previous();
            newHour = 24 - (hours % 24 - newHour );
        }
        else
            newHour -= hours;

        return new Time(newDay, newHour, newMinute);
    }
    public Time subtractTime(int minutes){
        int hours = minutes / 60;
        return subtractTime(hours, minutes % 60);
    }
}
