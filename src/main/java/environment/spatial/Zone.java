package environment.spatial;

import environment.ParkingMeter;

public enum Zone{

    A(6.0),
    B(5.0),
    C(4.0),
    None(0.0);

    /**
     * Price for an hour of parking in a particular zone.
     */
    private double price;

    private Zone(double ticketPrice){
        this.price = ticketPrice;
    }

    public double getPrice(){ return this.price; }

    public String toString(){
        return "'Krakow" + this.name() + "'";
    }
    public int getMinutesForMinimalPayment(){
        double min = ParkingMeter.minParkingPayment;
        double perMinute = this.price / 60.0;
        int result = (int) Math.ceil(min / perMinute);
        return result;
    }
}
