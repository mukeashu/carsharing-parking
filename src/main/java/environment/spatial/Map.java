package environment.spatial;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.lang.Math;
import java.util.Random;


public class Map{
    //placeholder values for approximate city limits

    private final double eastLimit = 20.2181;
    private final double westLimit = 19.79151;
    private final double southLimit = 49.96692;
    private final double northLimit = 50.12692;

/*    //values for center of Krakow
    private final double eastLimit = 19.93411;
    private final double westLimit = 19.93411;
    private final double southLimit = 50.0565;
    private final double northLimit = 50.0565;
*/
    private static EntityManager em;
    private Random rand;

    public static void main(String[] args){
        Point point = new Point(50.05729, 19.93518);
        Map map = new Map();

        //String queryText = "SELECT ST_COVERS((SELECT geo FROM zones WHERE name = 'KrakowA'),ST_GeogFromText('POINT(19.93518 50.05729)'));";
        String queryText = "SELECT multipoint FROM roads WHERE id='1';";
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Carsharing");
        em = emf.createEntityManager();
        Query query = em.createNativeQuery(queryText);

        //MultiPoint result = (MultiPoint) query.getSingleResult();

        //System.out.println(result);
        //System.out.println(map.isWithinZone(point, Zone.A));
    }

    public Map(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Carsharing");
        em = emf.createEntityManager();
        rand = new Random(System.currentTimeMillis());
    }


    public Point getRandomParkable(){
        double randX = westLimit + (eastLimit-westLimit) * rand.nextDouble();
        double randY = southLimit + (northLimit-southLimit) * rand.nextDouble();
        Point randomPoint = new Point(randX, randY);
        //System.out.println(randomPoint);
        //TODO add verification if point is within city limits
        //Zone pointZone = getZone(randomPoint);
        randomPoint.setZone();
        return randomPoint;
    }

    public Zone getZone(Point point){

        if(isWithinZone(point, Zone.A)){
            return Zone.A;
        }
        else if(isWithinZone(point, Zone.B)){
            return Zone.B;
        }
        else if(isWithinZone(point, Zone.C)){
            return Zone.C;
        }
        else
            return Zone.None;
    }

    public static boolean isWithinZone(Point point, Zone zone) {
        String pointText = point.toString();
        String zoneText = zone.toString();
        String queryText = "SELECT ST_COVERS((SELECT geo FROM zones WHERE name = " + zoneText + "),ST_GeogFromText(" + pointText + "));"; //should be good rn
        //System.out.println("queryText = " + queryText);
        Query query = em.createNativeQuery(queryText);

        return (Boolean) query.getSingleResult();

    }

    public static EntityManager getEm() {
        return em;
    }

    public static void setEm(EntityManager em) {
        Map.em = em;
    }


}