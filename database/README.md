# Running the database with the project

## Step by step installation instructions

1. Install PostegreSQL and PostGIS software on the system.
2. Use postgres account present on your system to have all rigths to modify access and accounts existing in the database and open psql:
```bash
sudo -su postgres
psql
```
6. In psql create a new database and a new user:
```sql
CREATE DATABASE carsharing;
CREATE ROLE karol WITH LOGIN PASSWORD '1234';
GRANT ALL PRIVILEGES ON DATABASE carsharing TO karol;
```
7. Install PostGIS:
```sql
USE DATABASE carsharing;
CREATE EXTENSION postgis;
```
8. Exit psql and test the database by loading the sql dump from the project:
```bash
\q
exit # in your system shell, end a session of user postgres
psql -d carsharing -U karol -f "path/to/sql/dump" # the sql dump in the project directory is under path Database/carsharing.sql 
```

*It is possible that the `carsharing.sql` will install postgis extension anyway*

### Summary 

* sql dump file = carsharing.sql
* database name = carsharing
* username = karol
* password = 1234
